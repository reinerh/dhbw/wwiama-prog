# wwiama-prog

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Fwwiama-prog/master?urlpath=lab)

Material zu den Programmiervorlesungen in den WWIAMA-Kursen im 1. und 2. Semester an der DHBW Mannheim.

## Links
- MyBinder: https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Fwwiama-prog/master?urlpath=lab
