# Folien

Dieses Verzeichnis enthält Vorlesungsfolien im TeX-Format.

## Struktur

- `src`: Enthält die eigentlichen LaTeX-Quelldateien
- `textools`: Enthält LaTeX-Pakete und ggf. Skripte, die für die Folien gebraucht werden. Die Dateien hier sollten generisch bleiben, ggf. werden sie in ein eigenes Repo ausgelagert, das von mehreren Kurs-Repos gemeinsam genutzt wird.
- Hauptverzeichnis: Haupt-Makefile und ggf. weitere kursspezifische Skripte 